package test.data_structures;

import static org.junit.Assert.assertEquals;

import org.junit.*;

import model.data_structures.*;

public class TestAlgoritmosOrdenamiento 
{

	private Arreglo<Integer> arreglo;
	
	private AlgoritmosOrdenamiento<Integer> ordenador;

	@Before
	public void setUp1()
	{
		arreglo = new Arreglo<Integer>(10);
		for (int i = 0; i < 10; i++)
		{
			arreglo.add(i + 1);
		}
		ordenador = new AlgoritmosOrdenamiento<Integer>();
	}
	
	public void setUp2()
	{
		arreglo = new Arreglo<Integer>(10);
		for (int i = 10; i > 0; i--)
		{
			arreglo.add(i);
		}
		ordenador = new AlgoritmosOrdenamiento<Integer>();
	}
	
	public void setUp3()
	{
		arreglo = new Arreglo<Integer>(10);
		for (int i = 0; i < 10; i++)
		{
			arreglo.add(i + 1);
		}
		arreglo.shuffle();
		ordenador = new AlgoritmosOrdenamiento<Integer>();
	}
	
	@Test
	public void testShellAscendente()
	{
		setUp1();
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
		ordenador.shellSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
	
	@Test
	public void testShellDescendente()
	{
		setUp2();
		assertEquals(10, (int)arreglo.get(0));
		assertEquals(1, (int)arreglo.get(arreglo.size() - 1));
		ordenador.shellSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
	
	@Test
	public void testShellRandom()
	{
		setUp3();
		ordenador.shellSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
	
	@Test
	public void testMergeAscendente()
	{
		setUp1();
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
		ordenador.mergeSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
	
	@Test
	public void testMergeDescendente()
	{
		setUp2();
		assertEquals(10, (int)arreglo.get(0));
		assertEquals(1, (int)arreglo.get(arreglo.size() - 1));
		ordenador.mergeSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
	
	@Test
	public void testMegeRandom()
	{
		setUp3();
		ordenador.mergeSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
	
	@Test
	public void testQuickAscendente()
	{
		setUp1();
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
		ordenador.quickSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
	
	@Test
	public void testQuickDescendente()
	{
		setUp2();
		assertEquals(10, (int)arreglo.get(0));
		assertEquals(1, (int)arreglo.get(arreglo.size() - 1));
		ordenador.mergeSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
	
	@Test
	public void testQuickRandom()
	{
		setUp3();
		ordenador.quickSort(arreglo);
		assertEquals(1, (int)arreglo.get(0));
		assertEquals(10, (int)arreglo.get(arreglo.size() - 1));
	}
}
