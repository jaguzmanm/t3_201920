package model.logic;

public class UBERTrip implements Comparable<UBERTrip>
{

	private int sourceID;
	
	private int dstID;
	
	private int hod;
	
	private double meanTT;
	
	private double standardDTT;
	
	private double geometricMTT;
	
	private double geometricSDTT;

	public UBERTrip (int sourceID, int dstID, int hod, double meanTT, double standardDTT, double geometricMTT, double geometricSDTT) 
	{
		this.sourceID = sourceID;
		this.dstID = dstID;
		this.hod = hod;
		this.meanTT = meanTT;
		this.standardDTT = standardDTT;
		this.geometricMTT = geometricMTT;
		this.geometricSDTT = geometricSDTT;
	}

	public int getSourceID()
	{
		return sourceID;
	}

	public void setSourceID(int sourceID) 
	{
		this.sourceID = sourceID;
	}

	public int getDstID() 
	{
		return dstID;
	}

	public void setDstID(int dstID) 
	{
		this.dstID = dstID;
	}

	public int getHod() {
		return hod;
	}

	public void setHod(int hod) 
	{
		this.hod = hod;
	}

	public double getMeanTT()
	{
		return meanTT;
	}

	public void setMeanTT(double meanTT) 
	{
		this.meanTT = meanTT;
	}

	public double getStandardDTT() 
	{
		return standardDTT;
	}

	public void setStandardDTT(double standardDTT) 
	{
		this.standardDTT = standardDTT;
	}

	public double getGeometricMTT() 
	{
		return geometricMTT;
	}

	public void setGeometricMTT(double geometricMTT) 
	{
		this.geometricMTT = geometricMTT;
	}

	public double getGeometricSDTT() 
	{
		return geometricSDTT;
	}

	public void setGeometricSDTT(double geometricSDTT) 
	{
		this.geometricSDTT = geometricSDTT;		
	}

	public int compareTo(UBERTrip pTrip) 
	{
		if (this.meanTT > pTrip.getMeanTT())
			return 1;
		if (this.meanTT < pTrip.getMeanTT())
			return -1;
		if (this.standardDTT > pTrip.getStandardDTT())
			return 1;
		if (this.standardDTT < pTrip.getStandardDTT())
			return -1;
		return 0;
	}
	
	public String toString()
	{
		return "Tiempo promedio: " + meanTT + ", Desviación estándar: " + standardDTT + 
				", Zona origen: " + sourceID + ", Zona destino: " + dstID + ", Hora: " + hod;
	}
}
