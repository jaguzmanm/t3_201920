package controller;

import java.util.Scanner;

import model.data_structures.*;
import model.logic.*;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.cargarCSV();
				System.out.println("--------------------------------------------------------------------------------------");
				System.out.println("Se leyeron " +  modelo.darTamanioDatos() + " viajes para el primer trimestre del a�o\n");
				UBERTrip primero = modelo.darPrimero();
				System.out.println("Primer viaje:\n-Zona origen: " + primero.getSourceID() + "\n-Zona destino: " + primero.getDstID() + 
						"\n-Hora: " + primero.getHod() + "\n-Tiempo promedio: " + primero.getMeanTT() + "\n");
				UBERTrip ultimo = modelo.darUltimo();
				System.out.println("�ltimo viaje:\n-Zona origen: " + ultimo.getSourceID() + "\n-Zona destino: " + ultimo.getDstID() + 
						"\n-Hora: " + ultimo.getHod() + "\n-Tiempo promedio: " + ultimo.getMeanTT());
				System.out.println("--------------------------------------------------------------------------------------\n");
				break;

			case 2:
				System.out.println("--------------------------------------------------------------------------------------");
				System.out.println("Ingrese la hora que desea consultar");
				int hora = lector.nextInt();
				boolean valido = false;
				while (!valido)
				{
					if (hora < 0 || hora > 23)
					{
						System.out.println("Ingrese una hora v�lida");
						hora = lector.nextInt();
					}
					else
						valido = true;
				}
				System.out.println("Se encontraron " + modelo.calcularDatosHora(hora).size() + " viajes en la hora dada");
				System.out.println("--------------------------------------------------------------------------------------\n");
				break;


			case 3:
				IArreglo<UBERTrip> arregloS = modelo.darDatosHora();
				if (arregloS.size() == 0)
					System.out.println("Primero se deben consultar los viajes para una hora dada");
				else
				{
					System.out.println("--------------------------------------------------------------------------------------");
					long duracion = modelo.shellSort();
					System.out.println("El algoritmo tard� " + duracion + " milisegundos ordenando los datos");
					System.out.println("\nPrimeros 10 viajes del arreglo:");
					for (int i = 0; i < 10; i++)
						System.out.println(arregloS.get(i));
					System.out.println("\nUltimos 10 viajes del arreglo:");
					for (int i = arregloS.size() - 10; i < arregloS.size(); i++)
						System.out.println(arregloS.get(i));
					System.out.println("--------------------------------------------------------------------------------------\n");
				}
				break;
			case 4:
				IArreglo<UBERTrip> arregloM = modelo.darDatosHora();
				if (arregloM.size() == 0)
					System.out.println("Primero se deben consultar los viajes para una hora dada");
				else
				{
					System.out.println("--------------------------------------------------------------------------------------");
					long duracion = modelo.mergeSort();
					System.out.println("El algoritmo tard� " + duracion + " milisegundos ordenando los datos");
					System.out.println("\nPrimeros 10 viajes del arreglo:");
					for (int i = 0; i < 10; i++)
						System.out.println(arregloM.get(i));
					System.out.println("\nUltimos 10 viajes del arreglo:");
					for (int i = arregloM.size() - 10; i < arregloM.size(); i++)
						System.out.println(arregloM.get(i));
					System.out.println("--------------------------------------------------------------------------------------\n");
				}
				break;
			case 5:
				IArreglo<UBERTrip> arregloQ = modelo.darDatosHora();
				if (arregloQ.size() == 0)
					System.out.println("Primero se deben consultar los viajes para una hora dada");
				else
				{
					System.out.println("--------------------------------------------------------------------------------------");
					long duracion = modelo.quickSort();
					System.out.println("El algoritmo tard� " + duracion + " milisegundos ordenando los datos");
					System.out.println("\nPrimeros 10 viajes del arreglo:");
					for (int i = 0; i < 10; i++)
						System.out.println(arregloQ.get(i));
					System.out.println("\nUltimos 10 viajes del arreglo:");
					for (int i = arregloQ.size() - 10; i < arregloQ.size(); i++)
						System.out.println(arregloQ.get(i));
					System.out.println("--------------------------------------------------------------------------------------\n");
				}
				break;
			case 6: 
				System.out.println("------------------- \n Hasta pronto !! \n-------------------"); 
				lector.close();
				fin = true;
				break;	

			default: 
				System.out.println("------------------- \n Opcion Invalida !! \n-------------------");
				break;
			}
		}

	}	
}
