package model.logic;


import model.data_structures.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.csvreader.*;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {

	private ILista<UBERTrip> datos;
	private IArreglo<UBERTrip> datosHora;
	private AlgoritmosOrdenamiento<UBERTrip> ordenador;
	
	public MVCModelo()
	{
		datos = new Lista<UBERTrip>();
		datosHora = new Arreglo<UBERTrip>(1000);
		ordenador = new AlgoritmosOrdenamiento<UBERTrip>();
	}

	public int darTamanioDatos()
	{
		return datos.darTamanio();
	}
	
	public IArreglo<UBERTrip> darDatosHora()
	{
		return datosHora;
	}

	public void agregar(UBERTrip dato)
	{	
		datos.agregar(dato);
	}
	
	public UBERTrip darPrimero()
	{
		return datos.darHead().darItem();
	}
	
	public UBERTrip darUltimo()
	{
		return datos.darTail().darItem();
	}
	
	public IArreglo<UBERTrip> calcularDatosHora(int pHora)
	{
		vaciarDatosHora();
		Node<UBERTrip> actual = datos.darHead();
		while (actual != null)
		{
			UBERTrip viaje = actual.darItem();
			if (viaje.getHod() == pHora)
			{
				datosHora.add(viaje);
			}
			actual = actual.next();				
		}
		return datosHora;
	}
	
	public long shellSort()
	{
		long startTime = System.currentTimeMillis();
		ordenador.shellSort(datosHora);
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		return duration;
	}
	
	public long mergeSort()
	{
		long startTime = System.currentTimeMillis();
		ordenador.mergeSort(datosHora);
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		return duration;
	}
	
	public long quickSort()
	{
		long startTime = System.currentTimeMillis();
		ordenador.quickSort(datosHora);
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		return duration;
	}
	
	private void vaciarDatosHora()
	{
		datosHora = null;
		datosHora = new Arreglo<UBERTrip>(1000);
	}
	
	public void cargarCSV() 
	{
		try
		{          
			CsvReader leerViajes = new CsvReader("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv");
			leerViajes.readHeaders();
			while(leerViajes.readRecord()) 
			{
				String strSourceID = leerViajes.get(0);
				String strDstID = leerViajes.get(1);
				String strHod = leerViajes.get(2);
				String strMeanTravelTime = leerViajes.get(3);
				String strStandardDeviationTravelTime = leerViajes.get(4);
				String strGeometricMeanTravelTime = leerViajes.get(5);
				String strGeometricStandardDeviationTravelTime = leerViajes.get(6);

				int sourceID = Integer.parseInt(strSourceID);
				int dstID = Integer.parseInt(strDstID);
				int hod = Integer.parseInt(strHod);
				double meanTravelTime = Double.parseDouble(strMeanTravelTime);
				double standardDeviationTravelTime = Double.parseDouble(strStandardDeviationTravelTime);
				double geometricMeanTravelTime = Double.parseDouble(strGeometricMeanTravelTime);
				double geometricStandardDeviationTravelTime = Double.parseDouble(strGeometricStandardDeviationTravelTime);

				UBERTrip actual = new UBERTrip(sourceID, dstID, hod, meanTravelTime, standardDeviationTravelTime, 
						geometricMeanTravelTime, geometricStandardDeviationTravelTime);
				agregar(actual);
			}
			leerViajes.close();
					
		} 
		catch(FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch(IOException e) 
		{
			e.printStackTrace();
		}
	}
}
