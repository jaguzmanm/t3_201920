package view;


public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar datos de los viajes UBER");
			System.out.println("2. Consultar viajes hora");
			System.out.println("3. Ordenar datos de la hora por Shell Sort");
			System.out.println("4. Ordenar datos de la hora por Merge Sort");
			System.out.println("5. Ordenar datos de la hora por Quick Sort");
			System.out.println("6. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
}
